import pytest

from .test_three import Task


@pytest.mark.kek
def test_asdict():
    t_task = Task('do something', 'okken', True, 21)
    t_dict = t_task._asdict()
    expected = {
        'summary': 'do something',
        'owner': 'okken',
        'done': True,
        'id': 21
    }
    assert t_dict == expected


def test_replace():
    t_before = Task('do something', 'okken', True, 21)
    t_after = t_before._replace(id=10, done=False)
    t_expected = Task('do something', 'okken', False, 10)
    assert t_after == t_expected
